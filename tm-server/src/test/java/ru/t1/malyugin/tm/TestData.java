package ru.t1.malyugin.tm;

import org.jetbrains.annotations.NotNull;
import ru.t1.malyugin.tm.api.service.IConnectionService;
import ru.t1.malyugin.tm.api.service.IPropertyService;
import ru.t1.malyugin.tm.api.service.IServiceLocator;
import ru.t1.malyugin.tm.api.service.model.IProjectService;
import ru.t1.malyugin.tm.api.service.model.ISessionService;
import ru.t1.malyugin.tm.api.service.model.ITaskService;
import ru.t1.malyugin.tm.api.service.model.IUserService;
import ru.t1.malyugin.tm.component.ServiceLocator;
import ru.t1.malyugin.tm.dto.model.SessionDTO;
import ru.t1.malyugin.tm.model.Project;
import ru.t1.malyugin.tm.model.Task;
import ru.t1.malyugin.tm.model.User;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public interface TestData {

    int NUMBER_OF_PROJECTS = 10;

    int NUMBER_OF_TASKS = 10;

    int NUMBER_OF_USERS = 10;

    int NUMBER_OF_SESSION = 10;

    @NotNull
    String FIRST_USUAL_USER_LOGIN = "TST_USER_1";

    @NotNull
    String FIRST_USUAL_USER_PASS = "TST_PASS_1";

    @NotNull
    String SECOND_USUAL_USER_LOGIN = "TST_USER_2";

    @NotNull
    String SECOND_USUAL_USER_PASS = "TST_PASS_2";

    @NotNull
    String UNKNOWN_ID = UUID.randomUUID().toString();

    @NotNull
    String UNKNOWN_STRING = "SOME_STR";

    @NotNull
    List<Project> PROJECT_LIST = new ArrayList<>();

    @NotNull
    List<Task> TASK_LIST = new ArrayList<>();

    @NotNull
    List<User> USER_LIST = new ArrayList<>();

    @NotNull
    List<SessionDTO> SESSION_LIST = new ArrayList<>();

    @NotNull
    IServiceLocator SERVICE_LOCATOR = new ServiceLocator();

    @NotNull
    IPropertyService PROPERTY_SERVICE = SERVICE_LOCATOR.getPropertyService();

    @NotNull
    ISessionService SESSION_SERVICE = SERVICE_LOCATOR.getSessionService();

    @NotNull
    ITaskService TASK_SERVICE = SERVICE_LOCATOR.getTaskService();

    @NotNull
    IProjectService PROJECT_SERVICE = SERVICE_LOCATOR.getProjectService();

    @NotNull
    IUserService USER_SERVICE = SERVICE_LOCATOR.getUserService();

    @NotNull
    IConnectionService CONNECTION_SERVICE = SERVICE_LOCATOR.getConnectionService();

}