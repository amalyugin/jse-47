package ru.t1.malyugin.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import ru.t1.malyugin.tm.api.repository.model.IWBSRepository;
import ru.t1.malyugin.tm.model.AbstractWBSModel;

import javax.persistence.EntityManager;
import java.util.List;

public abstract class AbstractWBSRepository<M extends AbstractWBSModel> extends AbstractUserOwnedRepository<M>
        implements IWBSRepository<M> {

    public AbstractWBSRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @NotNull
    @Override
    public List<M> findAllOrderByCreated(@NotNull final String userId) {
        @NotNull final String jpql = "SELECT m FROM " + getClazz().getSimpleName() + " m WHERE m.user.id = :userId ORDER BY m.created";
        return entityManager.createQuery(jpql, getClazz()).setParameter("userId", userId).getResultList();
    }

    @NotNull
    @Override
    public List<M> findAllOrderByName(@NotNull final String userId) {
        @NotNull final String jpql = "SELECT m FROM " + getClazz().getSimpleName() + " m WHERE m.user.id = :userId ORDER BY m.name";
        return entityManager.createQuery(jpql, getClazz()).setParameter("userId", userId).getResultList();
    }

    @NotNull
    @Override
    public List<M> findAllOrderByStatus(@NotNull final String userId) {
        @NotNull final String jpql = "SELECT m FROM " + getClazz().getSimpleName() + " m WHERE m.user.id = :userId ORDER BY m.status";
        return entityManager.createQuery(jpql, getClazz()).setParameter("userId", userId).getResultList();
    }

}