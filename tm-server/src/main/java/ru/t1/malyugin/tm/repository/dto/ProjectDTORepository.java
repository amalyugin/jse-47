package ru.t1.malyugin.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import ru.t1.malyugin.tm.api.repository.dto.IProjectDTORepository;
import ru.t1.malyugin.tm.dto.model.ProjectDTO;

import javax.persistence.EntityManager;

public final class ProjectDTORepository extends AbstractWBSDTORepository<ProjectDTO> implements IProjectDTORepository {

    public ProjectDTORepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @NotNull
    @Override
    public Class<ProjectDTO> getClazz() {
        return ProjectDTO.class;
    }

}
